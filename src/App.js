import './App.css';
import Studia from './Studia/Studia';

function App(props) {
  let number = props.lineBtns.length
  return (
    <div className="App">
        <Studia  type5={props.type5} number={number} acteveted={props.acteveted} typeBtns={props.typeBtns} 
                lineBtns={props.lineBtns} setSlide={props.setSlide} setBtn={props.setBtn} 
                editBtn={props.editBtn} deleteBtn={props.deleteBtn} changeBtns={props.changeBtns} 
                setNewType={props.setNewType} setType={props.setType} types={props.types} changeHeadType={props.changeHeadType}
                setlineBtnActive={props.setlineBtnActive} place={props.place} setHeadType={props.setHeadType} />
    </div>
  );
}

export default App;
