import  "./Slider.css"
import dotIcone from "./../images/3dot.png"

let Slider = (props) =>{

    const divStyle = {
        transform: `translateX(${props.position}px)`,
    }

    return(
        <div className="slideDiv" >
            <div className="slide" style={divStyle} >
                <img src={props.img} id="slideImg" />
                <div id="slideNameDot" >
                    <div>
                        {props.title}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Slider