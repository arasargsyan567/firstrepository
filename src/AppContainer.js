import App from "./App";
import {connect} from "react-redux"
import { changeBtns, deleteBtn, editBtn, setBtn, setlineBtnActive } from "./redux/studiaReduser";
import {  changeHeadType, setHeadType, setNewType, setSlide, setType, typeActiveted } from "./redux/headReduser";

function AppContainer(props) {

  return (
        <App  acteveted={props.typeActiveted} lineBtns={props.lineBtns} typeBtns={props.typeBtns} 
              slides={props.slides} setSlide={props.setSlide} setBtn={props.setBtn}
              editBtn={props.editBtn} deleteBtn={props.deleteBtn} changeBtns={props.changeBtns} 
              setType={props.setType} setNewType={props.setNewType} types={props.types} 
              setlineBtnActive={props.setlineBtnActive} place={props.place} setHeadType={props.setHeadType} changeHeadType={props.changeHeadType} />
  );
}

let mapStateToProps = (state) => {
  console.log(state)
  let newTypeBtns, newTypeSlider, place
  for (let i = 0; i < state.app.lineBtns.length; i++) {
    const element = state.app.lineBtns[i];
     let headType = "headType" + (element.id )
    
    if (element.areActive) {
      
      newTypeBtns =  state.head[headType].typeBtns
      newTypeSlider = state.head[headType].slider
      place = i + 1
    }
  }
  
  return{
      lineBtns: state.app.lineBtns,
      typeBtns: newTypeBtns ,
      types: newTypeSlider,
      place: place
  }
}

export default connect(mapStateToProps, {typeActiveted,  setBtn, editBtn, deleteBtn, changeBtns, changeHeadType,
                                          setSlide, setType, setNewType, setHeadType, setlineBtnActive } )(AppContainer);