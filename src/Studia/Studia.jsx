import "./Studia.css"
import Types from "../Types/Types"
import Slider from "./../slider/Slider"
import { useState } from "react"
import Addmenu from "../Types/common/Addmenu"
import Icone1 from "../images/Icone1"


const SetBtn = (props) =>{
    let clas = "icon" + props.btn.id
    let hovIconeStyle

    if (props.areActive) {
        hovIconeStyle = {
            backgroundColor: "rgba(245, 66, 47, 0.5)"
        }
    }

    return(
        <div className={clas} id="fullBtn"  >
            <div id="icone" >
                <Icone1 picture={props.btn.picture} color={props.btn.color}  />
            </div>
            <div className="btNameBar" > 
                <div onClick={() => {
                    props.setlineBtnActive(props.btn.id)
                    props.nulling()
                }} className="hovIconBtn" style={hovIconeStyle}  >
                    <div style={{backgroundColor: props.btn.color}} className="iconeBtn"></div>
                </div>
                <div className="btnName" >
                    <SetName changIsEdit={props.changIsEdit} setId={props.setId} id={props.btn.id} title={props.btn.title} />
                </div>
            </div>
        </div>
    )

}

const SetName = (props) =>{
    
    return(
        <div className="btnName" onClick={() => {
            props.changIsEdit(true)
            props.setId(props.id)
        }} >
            <div>{props.title}</div>
        </div>
    )
}


const Studia = (props) =>{
    
    let slides
    let [typeNumber, setTypeNumber] = useState(1)
    let [isAddNone, setIisAddNone] = useState(false)
    let [isEditNone, setIisEditNone] = useState(false)
    let [id, setId] = useState("")
    let [btns, setBtns] = useState(props.lineBtns)
    let [curentBtn, setCurentBtn] = useState(null)
    let isAdd = isAddNone
    let isEdit = isEditNone

    let changIsAdd = (are) =>{
        setIisAddNone(are)
    }

    let changIsEdit = (are) =>{
        setIisEditNone(are)
    }
    
    props.typeBtns && props.typeBtns.map(btn => {
        if (btn.acteved && typeNumber !== btn.id) {
            setTypeNumber(btn.id)
        }
    }) 

    let sliderTypeNumber = 0
    for (var prop in props.types) {

        sliderTypeNumber += 1
        if (sliderTypeNumber === typeNumber) {
            slides = props.types[prop]
        }
    }

    
    let [position, setposition] = useState(0)
    let maxRight 

    slides && (maxRight = (slides.length - 6) * 220)

    let nulling = () =>{
        setposition(0)
    }
    
    let moveLeft = ()=>{
        let newPosition = position - 220
        setposition(newPosition)
    }

    let moveRight = ()=>{
        let newPosition = position + 220
        setposition(newPosition)
    }

    let removeBtn1, removeBtn2
    let paddingNumber = "50px"

    if (position === 0) {
        removeBtn1 = "removeBtn"
        paddingNumber = "0px"
    }else if (position === -maxRight) {
        removeBtn2 = "removeBtn"
    }else {
        removeBtn1 = " "
        removeBtn2 = " "
    }
    if (maxRight < 0) {
        removeBtn2 = "removeBtn"
    }

    let dragEndHendler = (e) => {
    }

    let dragStartHendler = (e, btn) => {
        setCurentBtn(btn)
    }
    let dragLeavHendler = (e) => {

    }
    let dragOverHendler = (e) => {
        e.preventDefault()
    }
    let dropHendler = (e, btn) => {
        e.preventDefault()
        props.changeBtns(curentBtn, btn)
        props.changeHeadType(curentBtn, btn)
    }
    
    return(
        <div id="studia">
            <div className="bigContainer">
                <div id="pageName">
                    3D studia
                </div>
                <div id="proces" >
                    процессы
                </div>
                <div id="navbar">
                    <div id="container" >
                        <div id="lineBar">
                            <div id="line" >
                                
                                <div id="btnIcones" >
                                    {props.lineBtns.map(btn => {
                                        return <div 
                                        key={btn.id}
                                        draggable="true"
                                        onDragEnd={e => dragEndHendler(e)}
                                        onDragStart={e => dragStartHendler(e, btn)}
                                        onDragLeave={e => dragLeavHendler(e)}
                                        onDragOver={e => dragOverHendler(e)}
                                        onDrop={e => dropHendler(e, btn)} 
                                        >
                                                <SetBtn nulling={nulling} setlineBtnActive={props.setlineBtnActive}  changIsEdit={changIsEdit} 
                                                    setId={setId} btn={btn} areActive={btn.areActive} />
                                        </div>
                                        
                                    })}
                                </div>
                                        
                                <div onClick={() => changIsAdd(true)} id="navbarBtn" >+</div>
                            </div>
                            {isAdd && <Addmenu newBtn={props.setBtn} noDelate={"hey"} changing={changIsAdd} setHeadType={props.setHeadType} />}
                            {isEdit && <Addmenu newBtn={props.editBtn} yoDelate={"hey"} deleteBtn={props.deleteBtn} id={id} changing={changIsEdit} />}
                        </div>
                    </div>
                </div>
            </div>
            <Types  nulling={nulling} acteveted={props.acteveted} typeBtns={props.typeBtns} 
                    setSlide={props.setSlide} typeNumber={typeNumber} setType={props.setType}
                    setNewType={props.setNewType} place={props.place} />
            <div id="sliderContainer">
                <div >
                    <div style={{paddingLeft: paddingNumber}} id="slides" >
                        { slides && slides.map(slide =>{
                        return <Slider key={slide.id} position={position} img={slide.img} title={slide.title} />
                        })}
                    </div>
                    <div id="moveBtns" >
                        <div id={removeBtn1} className="slideBtnDiv1" onClick={() => moveRight()} >
                            <div className="slideMover" >{"<"}</div>
                        </div>
                        <div id={removeBtn2} className="slideBtnDiv2" onClick={() => moveLeft()} >
                            <div className="slideMover" >{">"}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{height: "100px"}} >
            </div>
        </div>
    )
}

export default Studia