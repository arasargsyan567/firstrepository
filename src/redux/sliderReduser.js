import slide1 from "./../images/slide1.jpg"
import slide2 from "./../images/slide2.jpg"
import slide3 from "./../images/slide3.jpg"
import slide4 from "./../images/slide4.jpg"


let initialState = {
    
    type1:[
        {img: slide1, title: "Ювелирный эскиз", id:1  },
        {img: slide1, title: "Ювелирный эскиз", id:2 },
        {img: slide1, title: "Ювелирный эскиз", id:3 },
        {img: slide1, title: "Ювелирный эскиз", id:4 },
        {img: slide1, title: "Ювелирный эскиз", id:5 },
        {img: slide1, title: "Ювелирный эскиз", id:6 },
        {img: slide1, title: "Ювелирный эскиз", id:7 },
    ],
    type2:[
        {img: slide2, title: "Ювелирный эскиз", id:1 },
        {img: slide1, title: "Ювелирный эскиз", id:2 },  
        {img: slide4, title: "Ювелирный эскиз", id:3 }, 
    ],
    type3:[
        {img: slide3, title: "Ювелирный эскиз", id:1 },
        {img: slide3, title: "Ювелирный эскиз", id:2 },
        {img: slide3, title: "Ювелирный эскиз", id:3 },
        {img: slide3, title: "Ювелирный эскиз", id:4 },
        {img: slide3, title: "Ювелирный эскиз", id:5 },
        {img: slide3, title: "Ювелирный эскиз", id:6 },
        {img: slide3, title: "Ювелирный эскиз", id:7 },
    ],
    type4:[
        {img: slide4, title: "Ювелирный эскиз", id:1 },
        {img: slide4, title: "Ювелирный эскиз", id:2 },
        {img: slide4, title: "Ювелирный эскиз", id:3 },
        {img: slide4, title: "Ювелирный эскиз", id:4 },
        {img: slide4, title: "Ювелирный эскиз", id:5 },
        {img: slide4, title: "Ювелирный эскиз", id:6 },
        {img: slide4, title: "Ювелирный эскиз", id:7 },
    ],
    type5: [
        {img: slide1, title: "Ювелирный эскиз", id:1 },
        {img: slide2, title: "Ювелирный эскиз", id:2  },
        {img: slide3, title: "Ювелирный эскиз", id:3  },
        {img: slide4, title: "Ювелирный эскиз", id:4  },  
        {img: slide4, title: "Ювелирный эскиз", id:5  },  
        {img: slide3, title: "Ювелирный эскиз", id:6  }, 
        {img: slide2, title: "Ювелирный эскиз", id:7  },  
        {img: slide1, title: "Ювелирный эскиз", id:8  },  
        {img: slide3, title: "Ювелирный эскиз", id:9  },   
        {img: slide2, title: "Ювелирный эскиз", id:10  },  
        {img: slide4, title: "Ювелирный эскиз", id:11  },  
        {img: slide1, title: "Ювелирный эскиз", id:12  }, 
    ],
}

const studiaReduser = (state = initialState, action) =>{
    switch (action.type) {
        case "SET_SLIDE":
            let newSlide = {
                img: action.img,
                title: action.title
            }
            
            
            let typeNumber = 0
            for (var prop in state) {

                typeNumber += 1
                if (typeNumber === action.place) {
                    
                    return {
                        ...state,
                        [prop]: [...state[prop], newSlide]
                    }
                }
            }


        case "SET_TYPE":
            var newTypeNumber = 0

            for (var prop in state) {
                newTypeNumber += 1
            }
            newTypeNumber += 1
            
            let newType = 'type' + newTypeNumber 

            return{
                ...state,
                [newType]: []
            }


        default:
            return state;
    }
} 



export default studiaReduser