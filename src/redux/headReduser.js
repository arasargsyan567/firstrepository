import Slider from "../slider/Slider"
import slide1 from "./../images/slide1.jpg"
import slide2 from "./../images/slide2.jpg"
import slide3 from "./../images/slide3.jpg"
import slide4 from "./../images/slide4.jpg"

let initialState = {
    headType1: {

        typeBtns: [
            {id:1, name: "Type 1", acteved:true },
            {id:2, name: "Type 2", acteved:false},
            {id:3, name: "Type 3", acteved:false},
            {id:4, name: "Type 4", acteved:false},
            {id:5, name: "Type 5", acteved:false}
        ],
        
        slider: {
    
            type1:[
                {img: slide1, title: "Ювелирный эскиз", id:1 },
                {img: slide1, title: "Ювелирный эскиз", id:2 },
                {img: slide1, title: "Ювелирный эскиз", id:3 },
                {img: slide1, title: "Ювелирный эскиз", id:4 },
            ],
            type2:[
                {img: slide2, title: "Ювелирный эскиз", id:1 },
                {img: slide1, title: "Ювелирный эскиз", id:2 },  
                {img: slide4, title: "Ювелирный эскиз", id:3 }, 
            ],
            type3:[
                {img: slide3, title: "Ювелирный эскиз", id:1 },
                {img: slide3, title: "Ювелирный эскиз", id:2 },
            ],
            type4:[
                {img: slide4, title: "Ювелирный эскиз", id:1 },
                {img: slide4, title: "Ювелирный эскиз", id:2 },
                {img: slide4, title: "Ювелирный эскиз", id:3 },
            ],
            type5: [
                {img: slide1, title: "Ювелирный эскиз", id:1 },
                {img: slide2, title: "Ювелирный эскиз", id:2  },
                {img: slide3, title: "Ювелирный эскиз", id:3  },
            ],
        }
    },

    headType2: {

        typeBtns: [
            {id:1, name: "Type 1", acteved:true },
            {id:2, name: "Type 2", acteved:false},
        ],

        slider: {
    
            type1:[
                {img: slide4, title: "Ювелирный эскиз", id:1  },
                {img: slide4, title: "Ювелирный эскиз", id:2 },
                {img: slide4, title: "Ювелирный эскиз", id:3 },
                {img: slide4, title: "Ювелирный эскиз", id:4 },
            ],
            type2:[
                {img: slide2, title: "Ювелирный эскиз", id:1 },
                {img: slide1, title: "Ювелирный эскиз", id:2 },  
                {img: slide4, title: "Ювелирный эскиз", id:3 }, 
            ],
        }
    },
    headType3: {

        typeBtns: [
            
            {id:1, name: "Type 1", acteved:true},
            {id:2, name: "Type 2", acteved:false},

        ],

        slider: {
    
            type1:[
                {img: slide3, title: "Ювелирный эскиз", id:1 },
                {img: slide3, title: "Ювелирный эскиз", id:2 },
            ],
            type2:[
                {img: slide4, title: "Ювелирный эскиз", id:1 },
                {img: slide4, title: "Ювелирный эскиз", id:2 },
                {img: slide4, title: "Ювелирный эскиз", id:3 },
            ]
        }
    },
    
    headType4: {

        typeBtns: [

        ],

        slider: {
    
        }
    },
    
    headType5: {
        
        typeBtns: [

        ],

        slider: {

        }
    },
    
    headType6: {
        
        typeBtns: [     

        ],

        slider: {

        }
    },
}

const headReduser = (state = initialState, action) =>{
    switch (action.type) {
        case "ACTIVATED":

            let typeNumber = 0
            let headType
            for (var prop in state) {
                
                typeNumber += 1
               
                if (typeNumber === action.place) {
                    
                    let newTypeBtns = []
                    newTypeBtns.push( state[prop].typeBtns.map(btn => {
                        headType = prop
                        if ( btn.id === action.id ) {
                            if (btn.acteved) {
                                btn.acteved = btn.acteved
                            }else{
                                btn.acteved = !btn.acteved
                            }
                            
                        }else{
                            btn.acteved = false
                        }
                        
                        return btn
                    }))
                    
                    return{
                        ...state,
                        [headType]: {typeBtns: newTypeBtns[0], slider: state[headType].slider }
                    }
                }
            }
            
            case "SET_NEW_TYPE":

                let typeNumber1 = 0
                let headType01
                for (var prop in state) {
                    
                    typeNumber1 += 1
                    headType01 = prop
                    if (typeNumber1 === action.place) {
                        
                        let newType = {id:state[headType01].typeBtns.length + 1, name: action.titel, acteved: false}
                        
                        return{
                            ...state,
                            [headType01]: {typeBtns: [ ...state[headType01].typeBtns, newType ], slider: state[headType01].slider }
                        }
                    }
                }

            case "SET_SLIDE":
                               
                let typeNumber02 = 0
                let headType02
                for (var prop1 in state) {
                    
                    typeNumber02 += 1
                    headType02 = prop1
                    if (typeNumber02 === action.place) {
                        
                        let newSlide = {
                            img: action.img,
                            title: action.title
                        }
                        
                        let typeNumber = 0
                        for (var prop in state[headType02].slider) {
            
                            typeNumber += 1
                            if (typeNumber === action.placeSlide) {

                                let newSlider = {
                                     ...state[headType02].slider,
                                     [prop]: [...state[headType02].slider[prop], newSlide]
                                    
                                }
                                
                                return {
                                    ...state,
                                    [headType02]: {typeBtns: state[headType02].typeBtns, slider: newSlider }
                                    
                                }
                            }
                        }
                        
                    }
                }
            
            case "SET_TYPE":

                let typeNumber03 = 0
                let headType03
                for (var prop in state) {
                    
                    typeNumber03 += 1
                    headType03 = prop
                    if (typeNumber03 === action.place) {
                        
                        var newTypeNumber = 0
    
                        for (var prop in state[headType03].slider) {
                            newTypeNumber += 1
                        }
                        newTypeNumber += 1
                        
                        let newType = 'type' + newTypeNumber 
                        let newSlider1 = {
                            ...state[headType03].slider,
                            [newType]: []
                           
                       }
            
                        return{
                            ...state,
                            [headType03]: {typeBtns: state[headType03].typeBtns, slider: newSlider1 }
                        }
                        
                    }
                }

            case "SET_HEAD_TYPE":

                var newHeadtypeNumber = 0

                for (var prop in state) {
                    newHeadtypeNumber += 1
                }
                newHeadtypeNumber += 1
            
                let newHeadtype = 'headType' + newHeadtypeNumber 

                return{
                    ...state,
                    [newHeadtype]: { typeBtns: [ ], slider: {} }
                }

            case "CHANGE_HEAD_TYPE":

                let startHeadType, endHeadType
                startHeadType = "headType" + action.startBtn.id
                endHeadType = "headType" + action.endBtn.id
                let changeHeadTypes = {}
                for (var prop in state) {
                    
                     if (prop === startHeadType) {

                        changeHeadTypes[endHeadType] = { typeBtns: state[startHeadType].typeBtns, slider: state[startHeadType].slider }
                        
                     }else if (prop === endHeadType) {

                        changeHeadTypes[startHeadType] = { typeBtns: state[endHeadType].typeBtns, slider: state[endHeadType].slider }
                    
                    }else {
                        changeHeadTypes[prop] = state[prop]
                     }

                }
                console.log(changeHeadTypes)
                return {
                        ...state
                    }
        default:
            return state;
    }
} 

export const typeActiveted = (id, place ) => { return ({ type: "ACTIVATED", id, place }) };
export const setNewType = (titel, place) => {return {type: "SET_NEW_TYPE", titel, place} }
export const setSlide = (img, title, placeSlide, place ) => {return({type:"SET_SLIDE", img, title, placeSlide, place })}
export const setType = ( place ) => {return({type:"SET_TYPE", place })}
export const setHeadType = ( ) => {return({type:"SET_HEAD_TYPE" })}
export const changeHeadType = (startBtn, endBtn ) => {return({type:"CHANGE_HEAD_TYPE", startBtn, endBtn })}



export default headReduser


                
                