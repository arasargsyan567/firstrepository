import { combineReducers, createStore } from "redux";
import studiaReduser from "./studiaReduser"
import typeReduser from "./typeReduser";
import SliderReduser from "./sliderReduser"
import headReduser from "./headReduser";
let redusers = combineReducers({
    app: studiaReduser,
    head: headReduser
})

let store = createStore(redusers)

export default store;