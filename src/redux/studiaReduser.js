


let initialState = {
    
    lineBtns: [
        {id:1, areActive: true, color: "#F5961B", picture: "icone1.svg", title: "Идеая Скетч",},
        {id:2, areActive: false, color: "#5181DF", picture: "icone2.svg", title: "Дизайн"},
        {id:3, areActive: false, color: "#F5421B", picture: "icone3.svg", title: "3D Файл"},
        {id:4, areActive: false, color: "#67AA82", picture: "icone4.svg", title: "Ренд"},
        {id:5, areActive: false, color: "#E2E47A", picture: "icone5.svg", title: "Анимация"},
        {id:6, areActive: false, color: "#BA68FB", picture: "icone6.svg", title: "Полировка"},
    ]
}

const studiaReduser = (state = initialState, action) =>{
    switch (action.type) {
        case "EDIT_BTN":

            let newBtns = []
            newBtns.push(state.lineBtns.map(btn =>{
                if (btn.id === action.id) {
                    action.img && (btn.picture = action.img)
                    action.title && (btn.title = action.title)
                    action.color && (btn.color = action.color)
                }
                return btn
            }))
            return{
                ...state,
                lineBtns: newBtns[0]
            }

        case "SET_BTN":

            let newBtn = {
                id: state.lineBtns.length + 1,
                areActive: false,
                color: action.color,
                picture: action.img,
                title: action.title,
            }
            return{
                ...state,
                lineBtns: [...state.lineBtns, newBtn]
            }

        case "DELETE_BTN":

        let removeBtns = []
        removeBtns = state.lineBtns.filter(btn => btn.id !== action.id)
        
            return {
                ...state,
                lineBtns: removeBtns
            }

        case "CHANGE_BTN":

            let changeBtns = []
            changeBtns = state.lineBtns.map(btn => {
                switch (btn.id) {
                    case action.endBtn.id:
                       return action.startBtn

                    case action.startBtn.id:
                        return action.endBtn
                        

                    default:
                        return btn;
                }
            })
            return {
                    ...state,
                    lineBtns: changeBtns
                }

        case "SET_ACTIVE":

            let newLineBtns = []
            newLineBtns.push( state.lineBtns.map(btn => {
                if (btn.id === action.id ) {
                    !btn.areActive && (btn.areActive = !btn.areActive)
                }else{
                    btn.areActive = false
                }
                
                return btn
            }))
            
            return{
                ...state,
                lineBtns: newLineBtns[0]
            }
        
        default:
            return state;
    }
} 

export const setBtn = (img, title, color) => {return({type:"SET_BTN", img, title, color })}
export const setlineBtnActive = (id) => {return({type:"SET_ACTIVE", id })}
export const editBtn = (img, title, color, id) => {return({type:"EDIT_BTN", img, title, color, id })}
export const deleteBtn = (id) => {return({type:"DELETE_BTN", id })}
export const changeBtns = (startBtn, endBtn ) => {return({type:"CHANGE_BTN", startBtn, endBtn })}


export default studiaReduser