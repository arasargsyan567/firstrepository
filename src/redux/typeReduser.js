
let initialState = {
    typeBtns: [
        {id:1, name: "Type 1", acteved:true },
        {id:2, name: "Type 2", acteved:false},
        {id:3, name: "Type 3", acteved:false},
        {id:4, name: "Type 4", acteved:false},
        {id:5, name: "Type 5", acteved:false}
    ]
}

const typeReduser = (state = initialState, action) =>{
    switch (action.type) {
        case "ACTIVATED":
            
            let newTypeBtns = []
            newTypeBtns.push( state.typeBtns.map(btn => {
                if (btn.id === action.id ) {
                    btn.acteved = !btn.acteved
                }else{
                    btn.acteved = false
                }
                
                return btn
            }))

            return{
                ...state,
                typeBtns: newTypeBtns[0]
            }
        case "SET_NEW_TYPE":

            let newType = {id:state.typeBtns.length + 1, name: action.titel, acteved: false}

            return{
                ...state,
                typeBtns: [...state.typeBtns, newType]
            }
        
        default:
            return state;
    }
} 

// export const typeActiveted = (id) => { return ({ type: "ACTIVATED", id }) };


export default typeReduser