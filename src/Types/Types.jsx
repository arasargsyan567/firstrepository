import react, { useState } from "react"
import fileimg from "./../images/imgIcone.png"
import "./Types.css"
import iconeArmenia from "./../images/iconeArmenia.jpg"
import iconeRussia from "./../images/iconeRussia.jpg"
import iconeUsa from "./../images/iconeUsa.png"
import Addmenu from "./common/Addmenu"

let SetTypeBtns = (props) =>{

    let btnBorderWith = 610 / (props.typeBtns.length )

    return(
        props.typeBtns.map(btn =>{
            let btnBorder 
                btn.acteved? btnBorder = "btnBorder" : btnBorder = "disActived"

            return (
                <div key={btn.id} style={{width: btnBorderWith}} className="typeBtBorder" >
                    <div className="typeBtn" >
                        <div onClick={() => {props.acteveted(btn.id, props.place)
                        props.nulling()} } > {btn.name} </div>
                    </div>
                    <div className={btnBorder} >
    
                    </div>
                </div>
            )
        })
    )
}

let Types = (props) =>{

    let [newSlideArmName, setNewSlideArmName] = useState("")
    let [newSlideRusName, setNewSlideRusName] = useState("")
    let [newSlideUsName, setNewSlideUsName] = useState("")
    let [lengwij, setLengwij] = useState(1)
    let [newSlideImg, setNewSlideImg] = useState("")
    let [newSlideFile, setNewSlideFile] = useState("")
    let [addin, setAdding] = useState(false)
    let [imgBgColor, setImgBgColor] = useState("#383A45")
    let [fileBgColor, setFileBgColor] = useState("#383A45")
    let [areTyping, setAreTyping] = useState(false)
    let adding = addin
    let isNone, isNone1, isNone2, flagNumber1, flagNumber2, flagNumber3, nameValue


    if (adding) {
        isNone = "closeBtn" 
        isNone2 = "closeBtn1" 
    }else{
        isNone1 = "closeBtn" 
        isNone2 = "closeBtn1"
    }

    if (adding === 1) {
        isNone1 = "closeBtn"
        isNone2 = " "
        
    }

    if (lengwij === 1) {
        flagNumber2 = ""
        flagNumber3 = ""
        flagNumber1 = "activeFlag"
        nameValue = newSlideArmName
    }else if (lengwij === 2) {
        flagNumber1 = ""
        flagNumber3 = ""
        flagNumber2 = "activeFlag"
        nameValue = newSlideRusName
    }else if (lengwij === 3) {
        flagNumber1 = ""
        flagNumber2 = ""
        flagNumber3 = "activeFlag"
        nameValue = newSlideUsName
    }

    let setNameValue = (e) =>{
        if (lengwij === 1) {
            setNewSlideArmName(e)
        }else if (lengwij === 2) {
            setNewSlideRusName(e)
        }else if (lengwij === 3) {
            setNewSlideUsName(e)
        }
    }

    let changeing = (yo) =>{
        if (yo) {
            setAdding(yo)
        }else{
            setAdding(!addin)
        }
    }

    let cleaning = () =>{
        setNewSlideImg(null)
        setNewSlideRusName(" ")
        setImgBgColor("#383A45")
        setFileBgColor("#383A45")
    }

    const readImages = (images) => {
        const readera = new FileReader();
        readera.readAsDataURL(images);
        readera.onload = (e) => {
            setNewSlideImg(e.target.result);
        };
        setImgBgColor("green")
    }

    const readFiles = (file) => {
        const readera = new FileReader();
        readera.readAsDataURL(file);
        readera.onload = (e) => {
            setNewSlideFile(e.target.result);
        };
        setFileBgColor("green")
    }

    let isAding
    if (newSlideRusName && newSlideImg) {
        isAding = true
    }else{
        isAding = false
    }
    
    return(
        <div id="bigTypeContainer" >
            <div id="typeBar">
                <div id="typeBtns" >
                    <SetTypeBtns place={props.place} nulling={props.nulling} acteveted={props.acteveted} typeBtns={props.typeBtns} />
                </div>
                <div id="typeLineBar" > 
                    <div onClick={() => setAreTyping(true) }  id="krestBtn" >
                        +
                    </div>
                </div>
            </div>
            <div id="typeAdder" >
                { areTyping && <Addmenu place={props.place} setType={props.setType} setNewType={props.setNewType} 
                                noDelate={"hey"} type="yes" changing={() => setAreTyping()} /> }
            </div>
            
            <div id="imgBar">
                    <button onClick={() => {changeing()}} className={"imgBtn " + isNone} >
                        <span id="imgBtnKrest" >+</span> 
                        <span>Добавить</span>
                    </button> 
                    <div className={"sliderAdit " + isNone1}  >
                       <div id="flagsWKrest" >
                            <div id="flags" >
                                <img onClick={() => setLengwij(1)} id={flagNumber1} src={iconeArmenia} className="flag" />
                                    &nbsp;
                                    &nbsp;
                                <img onClick={() => setLengwij(2)} id={flagNumber2} src={iconeRussia} className="flag" />
                                    &nbsp;
                                    &nbsp;
                                <img onClick={() => setLengwij(3)} id={flagNumber3} src={iconeUsa} className="flag" />
                            </div>
                            <div onClick={() => {
                                changeing()
                                cleaning()
                                }} className="aditKrest" >
                                &times;
                            </div>
                       </div>
                        
                        <div id="selects" >
                            <input id="saveFile"  type="file" onChange={event => readFiles(event.target.files[0])} />
                            <label className="file" style={{backgroundColor: fileBgColor}} htmlFor="saveFile">
                                <img src={fileimg} className="fileInput" />
                                <span className="inputName" >Добавить файл</span>
                            </label>
                            <input id="saveFoto"  type="file" onChange={event => readImages(event.target.files[0])} />
                            <label id="foto" className="file" style={{backgroundColor: imgBgColor}} htmlFor="saveFoto">
                                <img src={fileimg} className="fileInput" />
                                <span className="inputName" >Добавить Фото</span>
                            </label>
                        </div>
                        <div id="nameBar" >
                            <label htmlFor="addSliderName">
                                <span id="newSliderName" >
                                    Название
                                </span>
                            </label>
                            <input value={nameValue} onChange={event => setNameValue(event.target.value)} id="addSliderName" type="text" />
                            <button className="addSliderBtn" onClick={() => isAding? changeing(1) : () => {} }  >Сохранить</button>
                        </div>
                    </div>
                    <div className="newSlideMaket" id={isNone2} >
                        <div id="SlideMaketKrestDiv" >
                            <div id="slideMaketKrest" onClick={() =>{
                                changeing() 
                                cleaning()
                            } } >
                                &times;
                            </div>
                        </div>
                        <div id="newSlidMaketImgDiv" >
                            <img id="newSlidMaketImg" src={newSlideImg} alt="" />
                        </div>
                        <div id="newSlideMaketBtns" >
                            <span>
                                {newSlideRusName}
                            </span>
                            <button onClick={() => {
                                props.setSlide(newSlideImg, newSlideRusName, props.typeNumber, props.place)
                                changeing()
                                cleaning()
                            }} className="addSliderBtn" >
                                Открыт
                            </button>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default Types