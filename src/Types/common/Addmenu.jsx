import  "./Addmenu.css"
import fileimg from "./../../images/imgIcone.png"
import { useState } from "react"

let Addmenu = (props) =>{

    let [linBtnName, setLinBtnName] = useState("")
    let [linBtnImg, setLinBtnImg] = useState("")
    let [svgFileLabBg, setsvgLabBg] = useState("#383A45")
    let [svgColor, setSvgColor] = useState("")
    let delateStyle, addStyle

    if (props.noDelate) {
        delateStyle="divCleaner"
        addStyle="addBtnStyle"
    }else if (props.yoDelate) {
        delateStyle=""
        addStyle=""
    }

    const readSvgImages = (images) => {
        setLinBtnImg(images.name)
        setsvgLabBg("green")
    }

    let lineStyle, colorLabelStyle

    if (props.type) {
        lineStyle = {
            left: "-15px"
        }
        colorLabelStyle = {
            visibility: "hidden"
        }
    }

    return(
        <div style={lineStyle} id="lineAdder" >
            <div id="lineAddKrest" onClick={() => props.changing(false)} >
                &times;
            </div>
            <div style={colorLabelStyle} id="selectsDiv" >
                <input id="svgInput" type="file" onChange={e => readSvgImages(e.target.files[0])} />
                <label style={{backgroundColor: svgFileLabBg}} id="selectImg" htmlFor="svgInput">
                    <img src={fileimg} id="filesImg" />
                    <div id="selectsName" >Добавить Фото</div>
                </label>
            </div>
            <div id="nameBtn" >
                <div id="newNameLabel" >
                    Название
                    <input style={colorLabelStyle} type="color" value={svgColor} onChange={e => setSvgColor(e.target.value)} />
                </div>
                <input type="text" value={linBtnName} onChange={e => setLinBtnName(e.target.value)} id="newLineBtnName" />
               <div id="editBtns" >
                    <button id="delateBtn" className={delateStyle} onClick={() => {
                        props.deleteBtn(props.id) 
                        props.changing(false)
                    }} >
                        Delate
                    </button>
                    <button onClick={() => {
                        props.type && props.setType(props.place)
                        props.type && props.setNewType(linBtnName, props.place)
                        !props.type && props.newBtn(linBtnImg, linBtnName, svgColor, props.id)
                        !props.type && props.noDelate && props.setHeadType()
                        props.changing(false)
                        }} id="addBtn" className={addStyle}  >Добавить</button>
               </div>
            </div>
        </div>
    )
}

export default Addmenu